SPLC2019_Antenna_Apo-Games_SPL

This repository comprises a FeatureIDE project that can be imported as new project. It uses Antenna and provides a feature model as well as a set of implemented feature.

More details are explained in:

Jonas Akesson, Sebastian Nilsson, Jacob Kürger, Thorsten Berger: Migrating the Android Apo-Games into an Annotation-Based Software Product Line